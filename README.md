# check-sele-licenses

Simple executable to check the licenses of all dependencies in a cargo project or workspace. These
are compared with the list of approved licenses for the Silicon Economy project. If any license is
not approved (or missing), the program returns a non-zero exit code so this can be used in CI/CD
setups which should fail if unapproved licenses are accidentally used.

Uses https://crates.io/crates/cargo_metadata to extract the metadata (`cargo metadata`) and outputs
name, version and license for each package.

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
