// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

mod spdx_parser;

use cargo_metadata::Package;

use crate::spdx_parser::approve_non_compliant_licenses;
use std::io::Read;

/// Allowed packages, because of non-standard licenses
const ALLOW_LIST: &[&str] = &["u8g2-fonts"];

enum LicenseAppr {
    Unapproved(String),
    Approved,
    Missing,
}

fn main() -> Result<(), Vec<String>> {
    let cmd = cargo_metadata::MetadataCommand::new();
    let metadata = cmd.exec().unwrap();
    let mut missing_licenses = Vec::new();
    let mut unapproved_licenses = Vec::new();
    for p in metadata.packages {
        match check_package_license(p.clone()) {
            LicenseAppr::Approved => { /* Do nothing, license is approved */ }
            LicenseAppr::Unapproved(lic) => unapproved_licenses.push((p.name, lic)),
            LicenseAppr::Missing => missing_licenses.push(p.name),
        }
    }
    let mut check_failures = Vec::new();
    if !missing_licenses.is_empty() {
        check_failures.push(String::from("Some licenses are missing"));
        println!("\nMissing licenses:");
        for p in missing_licenses {
            println!("{}", p);
        }
    }
    if !unapproved_licenses.is_empty() {
        check_failures.push(String::from("Some licenses are not approved"));
        println!("\nUnapproved licenses:");
        for (p, l) in unapproved_licenses {
            println!("{} ({})", p, l);
        }
    }
    if !check_failures.is_empty() {
        println!();
        Err(check_failures)
    } else {
        Ok(())
    }
}

fn check_package_license(p: Package) -> LicenseAppr {
    if ALLOW_LIST.contains(&p.name.as_str()) {
        // Approved implicitly from allow list
        return LicenseAppr::Approved;
    }
    match (p.license.clone(), p.license_file()) {
        (Some(lic), _) => {
            println!("{} {} ({})", p.name, p.version, lic);
            if approve_non_compliant_licenses(&lic) {
                // Approved implicitly from non-compliant list
                return LicenseAppr::Approved;
            }
            match spdx_parser::parse(&lic) {
                Ok(license_identifier) => {
                    if !license_identifier.approved() {
                        LicenseAppr::Unapproved(lic)
                    } else {
                        // approved, as it was parsed and approved through approved list
                        LicenseAppr::Approved
                    }
                }
                Err(parse_error) => panic!("{:?}", vec![format!("{:?}", parse_error)]),
            }
        }
        (None, Some(lic_file)) => {
            let mut file = std::fs::File::open(&lic_file).unwrap_or_else(|e| {
                panic!(
                    "Can not open license file {lic_file} for package {}: {e}",
                    p.name
                )
            });
            let mut contents = String::new();
            file.read_to_string(&mut contents).unwrap_or_else(|e| {
                panic!(
                    "Can not read license file {lic_file} for package {}: {e}",
                    p.name
                )
            });
            if let Some(first_line) = contents.lines().next() {
                println!("{} {} ({})", p.name, p.version, first_line);
                let olf_licenses = [
                    "Open Logistics License",
                    "Open Logistics Foundation License",
                ];
                if olf_licenses.contains(&first_line) {
                    // Allow Open Logistics License
                    LicenseAppr::Approved
                } else {
                    LicenseAppr::Unapproved(first_line.to_string())
                }
            } else {
                println!("{} {} NO LICENSE INFORMATION", p.name, p.version);
                LicenseAppr::Missing
            }
        }
        _ => {
            println!("{} {} NO LICENSE INFORMATION", p.name, p.version);
            LicenseAppr::Missing
        }
    }
}
