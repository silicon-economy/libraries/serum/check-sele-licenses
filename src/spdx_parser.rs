// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use std::collections::VecDeque;
use std::fmt::{Debug, Display, Formatter};

/// Approved spdx licenses
const APPROVED_LICENSES: &[&str] = &[
    // Taken on 2022-09-05 from
    // https://oe160.iml.fraunhofer.de/wiki/pages/viewpage.action?spaceKey=HOW&title=Open+Source+and+Data+Licenses
    "0BSD",
    "BSD-2-Clause",
    "BSD-3-Clause",
    "AFL-2.1",
    "Apache-2.0",
    "Artistic-2.0",
    "BSL-1.0",
    "CC-BY-2.5",
    "CC-BY-3.0",
    "CC-BY-4.0",
    "CC0-1.0",
    "CDDL-1.0",
    "CDDL-1.1",
    "WTFPL",
    "EPL-1.0",
    "EPL-2.0",
    "GPL-2.0-with-classpath-exception",
    "LGPL-2.1",
    "LGPL-3.0",
    "HPND",
    "ISC",
    "JSON",
    "MIT",
    "MPL-2.0",
    "PSF-2.0",
    "Unicode-DFS-2016",
    "Unlicense",
    "Zlib",
    "ZPL-2.1",
];

/// Non compliant to spdx formating, but still approved licenses
const NON_COMPLIANT_APPROVED_LICENSES: &[&str] = &[
    // Something like "MIT/Apache-2.0" appears on crates.io but is not spdx compliant
    // Add additional non-compliant licenses here. These are getting checked before parsing and allow the parser to not do hacky workarounds
    "MIT/Apache-2.0",
    "Apache-2.0/MIT",
    "Unlicense/MIT",
    "MIT / Apache-2.0",
    "Apache-2.0/GPL-2.0+",
];

/// Trait that describes a full SPDX license identifier which can be one or a combination of
/// multiple licenses from the SPDX license list
pub trait LicenseIdentifier: Display + Debug {
    /// Checks if this license identifier is approved for use in Silicon Economy
    fn approved(&self) -> bool;
}

/// A single license, identified by its short identifier
#[derive(Debug)]
struct License {
    short_identifier: String,
}

impl Display for License {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.short_identifier)
    }
}

impl LicenseIdentifier for License {
    fn approved(&self) -> bool {
        APPROVED_LICENSES.contains(&self.short_identifier.as_str())
    }
}

/// Approves non-compliant licenses
pub fn approve_non_compliant_licenses(license_identifier: &str) -> bool {
    NON_COMPLIANT_APPROVED_LICENSES.contains(&license_identifier)
}

/// A combination of LICENSE-A AND LICENSE-B
#[derive(Debug)]
struct And {
    left: Box<dyn LicenseIdentifier>,
    right: Box<dyn LicenseIdentifier>,
}

impl Display for And {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "(AND {} {})", self.left, self.right)
    }
}

impl LicenseIdentifier for And {
    fn approved(&self) -> bool {
        self.left.approved() && self.right.approved()
    }
}

/// A combination of LICENSE-A OR LICENSE-B
#[derive(Debug)]
struct Or {
    left: Box<dyn LicenseIdentifier>,
    right: Box<dyn LicenseIdentifier>,
}

impl Display for Or {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "(OR {} {})", self.left, self.right)
    }
}

impl LicenseIdentifier for Or {
    fn approved(&self) -> bool {
        self.left.approved() || self.right.approved()
    }
}

#[derive(Debug)]
struct With {
    left: Box<dyn LicenseIdentifier>,
    right: Box<dyn LicenseIdentifier>,
}

impl Display for With {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "(WITH {} {}", self.left, self.right)
    }
}

impl LicenseIdentifier for With {
    fn approved(&self) -> bool {
        // To allow something like "X WITH Y OR Z" which could be approved if Z is approved, we
        // forward the decision to the right part of the WITH. All other WITH cases should be
        // denied. For the discussion, see:
        // https://gitlab.cc-asp.fraunhofer.de/silicon-economy/libraries/serum/check-sele-licenses/-/merge_requests/3#note_1097848
        self.right.approved()
    }
}

#[derive(PartialEq, Debug)]
enum Token {
    ShortId(String),
    And,
    Or,
    With,
    ParenL,
    ParenR,
}

#[derive(Debug, PartialEq)]
pub struct TokenizeError {
    rest: String,
}

fn tokenize(mut license_identifier: &str) -> Result<Vec<Token>, TokenizeError> {
    let mut tokens = Vec::new();
    while !license_identifier.is_empty() {
        if let Some(rest) = license_identifier.strip_prefix(" AND ") {
            license_identifier = rest;
            tokens.push(Token::And);
        } else if let Some(rest) = license_identifier.strip_prefix(" OR ") {
            license_identifier = rest;
            tokens.push(Token::Or);
        } else if let Some(rest) = license_identifier.strip_prefix(" WITH ") {
            license_identifier = rest;
            tokens.push(Token::With);
        } else if let Some(rest) = license_identifier.strip_prefix('(') {
            license_identifier = rest;
            tokens.push(Token::ParenL);
        } else if let Some(rest) = license_identifier.strip_prefix(')') {
            license_identifier = rest;
            tokens.push(Token::ParenR);
        } else if let Some(pos) = license_identifier.find(|c: char| !is_license_identifier_char(c))
        {
            if pos == 0 {
                return Err(TokenizeError {
                    rest: license_identifier.into(),
                });
            } else {
                let (short_id, rest) = license_identifier.split_at(pos);
                license_identifier = rest;
                tokens.push(Token::ShortId(short_id.into()));
            }
        } else {
            tokens.push(Token::ShortId(license_identifier.into()));
            license_identifier = "";
        }
    }
    Ok(tokens)
}

fn is_license_identifier_char(c: char) -> bool {
    c.is_ascii_alphanumeric() || c == '-' || c == '.'
}

#[derive(Debug, PartialEq)]
pub enum ParseError {
    Tokenize(TokenizeError),
    /// " AND ..." found, AND must be placed between two other license identifiers
    StartsWithAnd,
    /// " OR ..." found, OR must be placed between two other license identifiers
    StartsWithOr,
    /// " WITH ..." found, WITH must be placed between two other license identifiers
    StartsWithWith,
    /// ")" found, a right parenthesis is only allowed to close a previous left parenthesis
    StartsWithParenR,
    /// closing right parenthesis missing
    MissingParenR,
    /// Empty license identifier
    Empty,
    /// After parsing, there are tokens left
    TokensLeft,
}

/// Parses a license identifier string into a dyn LicenseIdentifier
///
/// Fails if parts of the string can not be tokenized, the string is empty (no tokens) or if there
/// are tokens left after parsing.
pub fn parse(license_identifier: &str) -> Result<Box<dyn LicenseIdentifier>, ParseError> {
    let tokens = tokenize(license_identifier).map_err(ParseError::Tokenize)?;
    let mut tokens = tokens.into();
    let license = parse_tokens(&mut tokens)?;
    if tokens.pop_front().is_some() {
        Err(ParseError::TokensLeft)
    } else {
        Ok(license)
    }
}

/// Parses a stream of tokens into a dyn LicenseIdentifier
fn parse_tokens(tokens: &mut VecDeque<Token>) -> Result<Box<dyn LicenseIdentifier>, ParseError> {
    if let Some(token) = tokens.pop_front() {
        match token {
            Token::ShortId(left_id) => {
                let left = Box::new(License {
                    short_identifier: left_id,
                });
                parse_and_or_and_with(tokens, left)
            }
            Token::And => Err(ParseError::StartsWithAnd),
            Token::Or => Err(ParseError::StartsWithOr),
            Token::With => Err(ParseError::StartsWithWith),
            Token::ParenL => {
                let left = parse_tokens(tokens)?;
                if let Some(Token::ParenR) = tokens.pop_front() {
                    parse_and_or_and_with(tokens, left)
                } else {
                    Err(ParseError::MissingParenR)
                }
            }
            Token::ParenR => Err(ParseError::StartsWithParenR),
        }
    } else {
        Err(ParseError::Empty)
    }
}

/// Both ShortId and parenthesized license identifiers can be followed by "AND" or "OR". So the
/// logic to handle these cases is extracted here to avoid code duplication. This function calls
/// `parse_tokens` again which closes the recursion.
fn parse_and_or_and_with(
    tokens: &mut VecDeque<Token>,
    left: Box<dyn LicenseIdentifier>,
) -> Result<Box<dyn LicenseIdentifier>, ParseError> {
    let next = tokens.get(0);
    if let Some(&Token::And) = next {
        tokens.pop_front().expect("Token::And expected");
        let right = parse_tokens(tokens)?;
        Ok(Box::new(And { left, right }))
    } else if let Some(&Token::Or) = next {
        tokens.pop_front().expect("Token::Or expected");
        let right = parse_tokens(tokens)?;
        Ok(Box::new(Or { left, right }))
    } else if let Some(&Token::With) = next {
        tokens.pop_front().expect("Token::With expected");
        let right = parse_tokens(tokens)?;
        Ok(Box::new(With { left, right }))
    } else {
        Ok(left)
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn tokenizer_success() {
        let tokens = tokenize("MIT").expect("this should tokenize correctly");
        let mut tokens = VecDeque::from(tokens);
        assert_eq!(
            tokens.pop_front().expect("MIT expected"),
            Token::ShortId("MIT".into())
        );
        assert_eq!(tokens.pop_front(), None);
        // this would not parse but it tokenizes correctly
        let tokens = tokenize("(MIT AND Apache-2.0)()").expect("this should tokenize correctly");
        let mut tokens = VecDeque::from(tokens);
        assert_eq!(tokens.pop_front().expect("( expected"), Token::ParenL);
        assert_eq!(
            tokens.pop_front().expect("MIT expected"),
            Token::ShortId("MIT".into())
        );
        assert_eq!(tokens.pop_front().expect("AND expected"), Token::And);
        assert_eq!(
            tokens.pop_front().expect("Apache-2.0 expected"),
            Token::ShortId("Apache-2.0".into())
        );
        assert_eq!(tokens.pop_front().expect(") expected"), Token::ParenR);
        assert_eq!(tokens.pop_front().expect("MIT expected"), Token::ParenL);
        assert_eq!(tokens.pop_front().expect("MIT expected"), Token::ParenR);
    }

    #[test]
    fn tokenizer_failure() {
        assert_eq!(
            tokenize(" ").unwrap_err(),
            TokenizeError { rest: " ".into() }
        );
        assert_eq!(
            tokenize("MIT Apache-2.0").unwrap_err(),
            TokenizeError {
                rest: " Apache-2.0".into()
            }
        );
        assert_eq!(
            tokenize("MIT_").unwrap_err(),
            TokenizeError { rest: "_".into() }
        );
        assert_eq!(
            tokenize("ö").unwrap_err(),
            TokenizeError { rest: "ö".into() }
        );
        assert_eq!(
            tokenize("MIT AND OR Apache-2.0").unwrap_err(),
            TokenizeError {
                rest: " Apache-2.0".into() // "OR" ist tokenized as ShortId
            }
        );
        assert_eq!(
            tokenize("(AND )").unwrap_err(),
            TokenizeError {
                rest: " )".into() // "AND" is tokenized as ShortId
            }
        );
    }

    #[test]
    fn parser_success() {
        // It is impossible to compare Box<dyn LicenseIdentifier>s so we check the string
        // representations instead
        assert_eq!(format!("{}", parse("MIT").unwrap()), "MIT");
        assert_eq!(
            format!("{}", parse("MIT AND Apache-2.0").unwrap()),
            "(AND MIT Apache-2.0)"
        );
        assert_eq!(
            format!("{}", parse("(MIT AND Apache-2.0)").unwrap()),
            "(AND MIT Apache-2.0)"
        );
        assert_eq!(
            format!("{}", parse("(((MIT AND Apache-2.0)))").unwrap()),
            "(AND MIT Apache-2.0)"
        );
        assert_eq!(
            format!("{}", parse("MIT AND (Apache-2.0 OR GPL-3.0-only)").unwrap()),
            "(AND MIT (OR Apache-2.0 GPL-3.0-only))"
        );
        assert_eq!(
            format!("{}", parse("(MIT AND Apache-2.0) OR GPL-3.0-only").unwrap()),
            "(OR (AND MIT Apache-2.0) GPL-3.0-only)"
        );
    }

    #[test]
    fn parser_failure() {
        assert_eq!(
            parse(" ").unwrap_err(),
            ParseError::Tokenize(TokenizeError { rest: " ".into() })
        );
        assert_eq!(parse(" AND MIT").unwrap_err(), ParseError::StartsWithAnd);
        assert_eq!(
            parse("(MIT AND Apache-2.0) OR ( OR GPL-3.0-only)").unwrap_err(),
            ParseError::StartsWithOr
        );
        assert_eq!(
            parse("(MIT AND Apache-2.0) AND ()").unwrap_err(),
            ParseError::StartsWithParenR
        );
        assert_eq!(
            parse("(MIT AND Apache-2.0").unwrap_err(),
            ParseError::MissingParenR
        );
        assert_eq!(parse("(").unwrap_err(), ParseError::Empty);
        assert_eq!(
            parse("(MIT OR Apache-2.0)MIT").unwrap_err(),
            ParseError::TokensLeft
        );
    }
}
